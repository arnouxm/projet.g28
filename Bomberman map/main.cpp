#include <SFML/Graphics.hpp>
#include "fonctions.hpp"
#define taille 15
#define tailleimage 50
#define PERSO1 "perso1_normal.gif"
#define PERSO1_BAS "perso1_bas.gif"
#define PERSO1_LARGEUR 15
#define PERSO1_HAUTEUR 27
#define PERSO2 "perso2_normal.gif"
#define PERSO2_LARGEUR 15
#define PERSO2_HAUTEUR 27
#define LARGEUR_APP 750
#define HAUTEUR_APP 750
#define PAS 50
using namespace sf;

typedef struct
{
    int x = 0, y = 0;
} Position;
Position posPerso1, posPerso2;

/*void coordonneePerso(int *x, int *y, int origine1, int origine2, int tailleimages)
{
    *x = origine1 + tailleimages;
    *y = origine2 + tailleimages;
}*/
Event evenement;
int main()
{
    RenderWindow app(sf::VideoMode(LARGEUR_APP,HAUTEUR_APP), "Map");
    Texture bloc1;
    Texture bloc2;
    Texture sol;
    int i,j;

    Texture perso1;
    Texture basPerso1;
    Texture perso2;

    if (!perso1.loadFromFile(PERSO1))
        printf("Probleme chargement de l'image du %s", PERSO1);
    if (!basPerso1.loadFromFile(PERSO1_BAS))
        printf("Probleme chargement de l'image du %s", PERSO1_BAS);
    if (!perso2.loadFromFile(PERSO2))
        printf("Probleme chargement de l'image du %s", PERSO2);
    Sprite perso1_normal, perso1_bas;
    Sprite perso2_normal;
    perso1_normal.setTexture(perso1);
    perso1_bas.setTexture(basPerso1);
    perso2_normal.setTexture(perso2);

    if(!bloc1.loadFromFile("bloca incassable.png"))
        printf("Probl�me de chargement de l'image");
    if(!bloc2.loadFromFile("bloc cassable.png"))
        printf("Probl�me de chargement de l'image");
    if(!sol.loadFromFile("texture sol.png"))
        printf("Probl�me de chargement de l'image");

    Sprite ts[15][15];
    Sprite sol1;
    sol1.setTexture(sol);

    Sprite blocindestructible;
    blocindestructible.setTexture(bloc1);

    Sprite blocdestructible;
    blocdestructible.setTexture(bloc2);

    perso1_normal.setOrigin(PERSO1_LARGEUR / 2, PERSO1_HAUTEUR / 2);
    coordonneePerso(&posPerso1.x, &posPerso1.y, 0, 0, tailleimage / 2);
    perso1_normal.setPosition(posPerso1.x, posPerso1.y);

    perso2_normal.setOrigin(PERSO2_LARGEUR / 2, PERSO2_HAUTEUR / 2);
    coordonneePerso(&posPerso2.x, &posPerso2.y, LARGEUR_APP, HAUTEUR_APP, -tailleimage/2);
    perso2_normal.setPosition(posPerso2.x, posPerso2.y);

    int res;
    titre(&res);
    printf("%i", res);
    if (res != 2)
        return EXIT_SUCCESS;

    while (app.isOpen())
    {

        int tilemap[taille][taille]=
        {
            1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
            1,2,0,2,0,2,0,2,0,2,0,2,0,2,1,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            1,2,0,2,0,2,0,2,0,2,0,2,0,2,1,
            1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
        };
        for(j=0; j<taille; j++)
        {
            for(i=0; i<taille; i++)
            {
                if (tilemap[j][i]==1)
                {
                    ts[j][i].setTexture(sol);
                    ts[j][i].setPosition(i*tailleimage,j*tailleimage);
                    app.draw(ts[j][i]);
                }
                if (tilemap[j][i]==2)

                {
                    ts[j][i].setTexture(bloc1);
                    ts[j][i].setPosition(i*tailleimage,j*tailleimage);
                    app.draw(ts[j][i]);

                }
                if (tilemap[j][i]==0)
                {
                    ts[j][i].setTexture(bloc2);
                    ts[j][i].setPosition(i*tailleimage,j*tailleimage);
                    app.draw(ts[j][i]);
                }
            }
        }
        while (app.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::Closed:
                app.close();
                break;
            case Event::KeyPressed:
                //D�placement perso 1
                if (evenement.key.code == Keyboard::D)
                {
                    app.clear();
                    if (posPerso1.x + PAS <= LARGEUR_APP && tilemap[caseJoueur(posPerso1.y)][caseJoueur(posPerso1.x + tailleimage)] == 1)
                        posPerso1.x += PAS;
                    perso1_normal.setPosition(posPerso1.x, posPerso1.y);
                    printf("%i, %i\n", posPerso1.x, posPerso1.y);
                    app.draw(perso1_bas);
                    app.display();
                }
                else if (evenement.key.code == Keyboard::S)
                {
                    app.clear();
                    if (posPerso1.y + PAS <= HAUTEUR_APP && tilemap[caseJoueur(posPerso1.y + tailleimage)][caseJoueur(posPerso1.x)] == 1)
                        posPerso1.y += PAS;
                    perso1_normal.setPosition(posPerso1.x, posPerso1.y);
                    printf("%i, %i\n", posPerso1.x, posPerso1.y);
                    app.draw(perso1_bas);
                    app.display();
                }
                else if (evenement.key.code == Keyboard::Q)
                {
                    app.clear();
                    if (posPerso1.x - PAS >= 0 && tilemap[caseJoueur(posPerso1.y)][caseJoueur(posPerso1.x - tailleimage)] == 1)
                        posPerso1.x -= PAS;
                    perso1_normal.setPosition(posPerso1.x, posPerso1.y);
                    printf("%i, %i\n", posPerso1.x, posPerso1.y);
                    app.draw(perso1_normal);
                    app.display();
                }
                else if (evenement.key.code == Keyboard::Z)
                {
                    app.clear();
                    if (posPerso1.y - PAS >= 0 && tilemap[caseJoueur(posPerso1.y - tailleimage)][caseJoueur(posPerso1.x)] == 1)
                        posPerso1.y -= PAS;
                    perso1_normal.setPosition(posPerso1.x, posPerso1.y);
                    printf("%i, %i\n", posPerso1.x, posPerso1.y);
                    app.draw(perso1_normal);
                    app.display();
                }

                //D�placement Perso 2
                if (evenement.key.code == Keyboard::Right)
                {
                    app.clear();
                    if (posPerso2.x + PAS <= LARGEUR_APP && tilemap[caseJoueur(posPerso2.y)][caseJoueur(posPerso2.x + tailleimage)] == 1)
                        posPerso2.x += PAS;
                    perso2_normal.setPosition(posPerso2.x, posPerso2.y);
                    printf("%i, %i\n", posPerso2.x, posPerso2.y);
                    app.draw(perso2_normal);
                    app.display();
                }
                else if (evenement.key.code == Keyboard::Down)
                {
                    app.clear();
                    if (posPerso2.y + PAS <= HAUTEUR_APP && tilemap[caseJoueur(posPerso2.y + tailleimage)][caseJoueur(posPerso2.x)] == 1)
                        posPerso2.y += PAS;
                    perso2_normal.setPosition(posPerso2.x, posPerso2.y);
                    printf("%i, %i\n", posPerso2.x, posPerso2.y);
                    app.draw(perso2_normal);
                    app.display();
                }
                else if (evenement.key.code == Keyboard::Left)
                {
                    app.clear();
                    if (posPerso2.x - PAS >= 0 && tilemap[caseJoueur(posPerso2.y)][caseJoueur(posPerso2.x - tailleimage)] == 1)
                        posPerso2.x -= PAS;
                    perso2_normal.setPosition(posPerso2.x, posPerso2.y);
                    printf("%i, %i\n", posPerso2.x, posPerso2.y);
                    app.draw(perso2_normal);
                    app.display();
                }
                else if (evenement.key.code == Keyboard::Up)
                {
                    app.clear();
                    if (posPerso2.y - PAS >= 0 && tilemap[caseJoueur(posPerso2.y - tailleimage)][caseJoueur(posPerso2.x)] == 1)
                        posPerso2.y -= PAS;
                    perso2_normal.setPosition(posPerso2.x, posPerso2.y);
                    printf("%i, %i\n", posPerso2.x, posPerso2.y);
                    app.draw(perso2_normal);
                    app.display();
                }
                break;
            }

        }
        app.draw(perso1_normal);
        app.draw(perso2_normal);
        app.display();

    }
    return 0;

}






