#include <SFML/Graphics.hpp>
#define LARGEUR_TITRE 1042
#define HAUTEUR_TITRE 586
#define PAGE_GARDE "titre2.png"
#define CREDIT "credit.png"
#define PLAY_HAUTEUR 401
#define PLAY_LARGEUR 339
#define QUIT_HAUTEUR 398
#define QUIT_LARGEUR 337
#define CREDIT_HAUTEUR 41
#define CREDIT_LARGEUR 82
using namespace sf;

typedef struct
{
    int x, y;
} Position;
Position mouse, playOrigin, quitOrigin,creditOrigin;

Event event;



void titre(int *res)
{

    RenderWindow fenetre(VideoMode(LARGEUR_TITRE, HAUTEUR_TITRE), "Titre");

    Texture premierPlan;
    Texture credit;

    if (!premierPlan.loadFromFile(PAGE_GARDE))
        printf("Erreur Image %s", PAGE_GARDE);
    if(!credit.loadFromFile(CREDIT))
        printf("Erreur Image %s", CREDIT);


    Sprite titre;
    titre.setTexture(premierPlan);
    Sprite credit1;
    credit1.setTexture(credit);
    while (fenetre.isOpen())
    {
        mouse.x = event.mouseMove.x;
        mouse.y = event.mouseMove.y;
        playOrigin.x = 607;
        playOrigin.y = 152;
        quitOrigin.x = 140;
        quitOrigin.y = 104;
        creditOrigin.x = 20;
        creditOrigin.y = 39;
        printf("%i, %i\n", mouse.x, mouse.y);


        while (fenetre.pollEvent(event))
        {

            switch (event.type)
            {
            case Event::Closed:
                fenetre.close();
                break;

            case Event::MouseButtonPressed:
                if (event.mouseButton.button == Mouse::Left)
                {
                    if (mouse.x >= playOrigin.x && mouse.x <= playOrigin.x + PLAY_LARGEUR)
                    {
                        if (mouse.y >= playOrigin.y && mouse.y <= playOrigin.y + PLAY_HAUTEUR)
                        {
                            fenetre.close();
                            *res = 2;
                            break;

                        }
                    }
                    if (mouse.x >= quitOrigin.x && mouse.x <= quitOrigin.x + QUIT_LARGEUR)
                    {
                        if (mouse.y >= quitOrigin.y && mouse.y <= quitOrigin.y + QUIT_HAUTEUR)
                        {
                            fenetre.close();
                            *res = 0;
                            break;

                        }
                    }
                    if (mouse.x >= creditOrigin.x && mouse.x <= creditOrigin.x + CREDIT_LARGEUR)
                    {
                        if (mouse.y >= creditOrigin.y && mouse.y <= creditOrigin.y + CREDIT_HAUTEUR)
                        {
                            fenetre.clear();
                            fenetre.draw(credit1);
                            fenetre.display();
                            sleep(seconds(3));
                        }
                    }

                }
            }
            fenetre.draw(titre);
            fenetre.display();
        }
    }
}
