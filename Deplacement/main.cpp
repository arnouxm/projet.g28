#include <SFML/Graphics.hpp>
#define PERSO1 "perso1_normal.gif"
#define PERSO1_BAS "perso1_bas.gif"
#define PERSO1_LARGEUR 15
#define PERSO1_HAUTEUR 27
#define PERSO2 "perso2_normal.gif"
#define PERSO2_LARGEUR 15
#define PERSO2_HAUTEUR 27
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 600
#define PAS 10
using namespace sf;

typedef struct
{
    int x = 0, y = 0;
}Position;
Position posPerso1, posPerso2;
Event evenement;

void coordonneePerso(int *x, int *y, int oriX, int oriY, int largeurPerso, int hauteurPerso)
{
    *x =  oriX + largeurPerso / 2;
    *y =  oriY + hauteurPerso / 2;
}

int main()
{
    RenderWindow fenetre(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "BomberMan");


    Texture perso1;
    Texture basPerso1;
    Texture perso2;

    if (!perso1.loadFromFile(PERSO1))
        printf("Probleme chargement de l'image du %s", PERSO1);
    if (!basPerso1.loadFromFile(PERSO1_BAS))
        printf("Probleme chargement de l'image du %s", PERSO1_BAS);
    if (!perso2.loadFromFile(PERSO2))
        printf("Probleme chargement de l'image du %s", PERSO2);
    Sprite perso1_normal, perso1_bas;
    Sprite perso2_normal;
    perso1_normal.setTexture(perso1);
    perso1_bas.setTexture(basPerso1);
    perso2_normal.setTexture(perso2);

    perso1_normal.setOrigin(PERSO1_LARGEUR / 2, PERSO1_HAUTEUR / 2);
    coordonneePerso(&posPerso1.x, &posPerso1.y, 0, 0, PERSO1_LARGEUR, PERSO1_HAUTEUR);
    perso1_normal.setPosition(posPerso1.x, posPerso1.y);

    perso2_normal.setOrigin(PERSO2_LARGEUR / 2, PERSO2_HAUTEUR / 2);
    coordonneePerso(&posPerso2.x, &posPerso2.y, LARGEUR_FENETRE, HAUTEUR_FENETRE, (- PERSO2_LARGEUR), (- PERSO2_HAUTEUR));
    perso2_normal.setPosition(posPerso2.x, posPerso2.y);

    while (fenetre.isOpen())
    {
        while (fenetre.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::Closed:
                fenetre.close();
                break;
            case Event::KeyPressed:
                //Déplacement perso 1
                if (evenement.key.code == Keyboard::D)
                {
                    fenetre.clear();
                    posPerso1.x += PAS;
                    if (posPerso1.x >= LARGEUR_FENETRE - PERSO1_LARGEUR )
                        posPerso1.x -= PAS;
                    perso1_normal.setPosition(posPerso1.x, posPerso1.y);
                    printf("%i, %i\n", posPerso1.x, posPerso1.y);
                    fenetre.draw(perso1_bas);
                    fenetre.display();
                }
                else if (evenement.key.code == Keyboard::S)
                {
                    fenetre.clear();
                    posPerso1.y += PAS;
                    if (posPerso1.y + PERSO1_HAUTEUR / 2 >= HAUTEUR_FENETRE)
                        posPerso1.y -= PAS;
                    perso1_normal.setPosition(posPerso1.x, posPerso1.y);
                    printf("%i, %i\n", posPerso1.x, posPerso1.y);
                    fenetre.draw(perso1_bas);
                    fenetre.display();
                }
                else if (evenement.key.code == Keyboard::Q)
                {
                    fenetre.clear();
                    posPerso1.x -= PAS;
                    if (posPerso1.x <= 0)
                        posPerso1.x += PAS;
                    perso1_normal.setPosition(posPerso1.x, posPerso1.y);
                    printf("%i, %i\n", posPerso1.x, posPerso1.y);
                    fenetre.draw(perso1_normal);
                    fenetre.display();
                }
                else if (evenement.key.code == Keyboard::Z)
                {
                    fenetre.clear();
                    posPerso1.y -= PAS;
                    if (posPerso1.y <= 0 + PERSO1_LARGEUR / 2)
                        posPerso1.y += PAS;
                    perso1_normal.setPosition(posPerso1.x, posPerso1.y);
                    printf("%i, %i\n", posPerso1.x, posPerso1.y);
                    fenetre.draw(perso1_normal);
                    fenetre.display();
                }

                //Déplacement Perso 2
                if (evenement.key.code == Keyboard::Right)
                {
                    fenetre.clear();
                    posPerso2.x += PAS;
                    if (posPerso2.x >= LARGEUR_FENETRE)
                        posPerso2.x -= PAS;
                    perso2_normal.setPosition(posPerso2.x, posPerso2.y);
                    printf("%i, %i\n", posPerso2.x, posPerso2.y);
                    fenetre.draw(perso2_normal);
                    fenetre.display();
                }
                else if (evenement.key.code == Keyboard::Down)
                {
                    fenetre.clear();
                    posPerso2.y += PAS;
                    if (posPerso2.y + PERSO2_HAUTEUR / 2 >= HAUTEUR_FENETRE)
                        posPerso2.y -= PAS;
                    perso2_normal.setPosition(posPerso2.x, posPerso2.y);
                    printf("%i, %i\n", posPerso2.x, posPerso2.y);
                    fenetre.draw(perso2_normal);
                    fenetre.display();
                }
                else if (evenement.key.code == Keyboard::Left)
                {
                    fenetre.clear();
                    posPerso2.x -= PAS;
                    if (posPerso2.x - PERSO2_LARGEUR / 2 <= 0)
                        posPerso2.x += PAS;
                    perso2_normal.setPosition(posPerso2.x, posPerso2.y);
                    printf("%i, %i\n", posPerso2.x, posPerso2.y);
                    fenetre.draw(perso2_normal);
                    fenetre.display();
                }
                else if (evenement.key.code == Keyboard::Up)
                {
                    posPerso2.y -= PAS;
                    fenetre.clear();
                    if (posPerso2.y <= 0 + PERSO2_HAUTEUR / 2)
                        posPerso2.y += PAS;

                    perso2_normal.setPosition(posPerso2.x, posPerso2.y);
                    printf("%i, %i\n", posPerso2.x, posPerso2.y);
                    fenetre.draw(perso2_normal);
                    fenetre.display();
                }


                break;
            }

        }
        fenetre.clear();
        fenetre.draw(perso1_normal);
        fenetre.draw(perso2_normal);
        fenetre.display();
    }

    return EXIT_SUCCESS;
}
